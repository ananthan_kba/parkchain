# ParkChain

The ParkChain smart contract governs the parking lot space faire calculation and profit partitioning, along with parking space slot allocation using ERC721 token standard.

For each parking lot registered to the application a contract will be deployed to the blockchain network with faire calculation decision parameters and percentage of profit partitioning, these will be immutable until the contract duration ends, or the contract is made invalid.

When a customer choice a parking space and books a slot, he will be given a PC token (an ERC721 standard based), which will be used up when customer uses the slot for the booked location.
